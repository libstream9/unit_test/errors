#include <stream9/errors/json.hpp>

#include "error_code.hpp"
#include "errors.hpp"
#include "namespace.hpp"

#include <string>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

using stream9::errors::error;
using std::string;

BOOST_AUTO_TEST_SUITE(json_)

    BOOST_AUTO_TEST_CASE(error_1_)
    {
        error e { "test", errc::error1 };

        auto v = json::value_from(e);

        json::object expected {
            { "what", "test" },
            { "why", {
                { "value", 0 },
                { "category", "test category" },
                { "message", "error1" },
            }},
            { "where", {
                { "filename", "../src/json.cpp" },
                { "function name", "void testing::json_::error_1_::test_method()" },
                { "line", 22 },
                { "column", 40 },
            }},
            { "context", json::object{} },
        };

        BOOST_TEST(v == expected);
    }

    BOOST_AUTO_TEST_CASE(error_2_)
    {
        try {
            try {
                throw error1 { "first", errc::error1 };
            }
            catch (...) {
                try {
                    std::throw_with_nested(error2 { "second", errc::error2 });
                }
                catch (...) {
                    std::throw_with_nested(error3 { "third", errc::error3 });
                }
            }
        }
        catch (error const& e) {
            auto v = json::value_from(e);

            json::array expected {
                {
                    { "what", "third" },
                    { "why", {
                        { "value", 2 },
                        { "category", "test category" },
                        { "message", "error3" },
                    }},
                    { "where", {
                        { "filename", "../src/json.cpp" },
                        { "function name", "void testing::json_::error_2_::test_method()" },
                        { "line", 56 },
                        { "column", 75 },
                    }},
                    { "context", json::object{} },
                },
                {
                    { "what", "second" },
                    { "why", {
                        { "value", 1 },
                        { "category", "test category" },
                        { "message", "error2" },
                    }},
                    { "where", {
                        { "filename", "../src/json.cpp" },
                        { "function name", "void testing::json_::error_2_::test_method()" },
                        { "line", 53 },
                        { "column", 76 },
                    }},
                    { "context", json::object{} },
                },
                {
                    { "what", "first" },
                    { "why", {
                        { "value", 0 },
                        { "category", "test category" },
                        { "message", "error1" },
                    }},
                    { "where", {
                        { "filename", "../src/json.cpp" },
                        { "function name", "void testing::json_::error_2_::test_method()" },
                        { "line", 49 },
                        { "column", 54 },
                    }},
                    { "context", json::object{} },
                },
            };

            BOOST_TEST(v == expected);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // json_

} // namespace testing
