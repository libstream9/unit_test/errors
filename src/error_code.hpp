#ifndef STREAM9_ERRORS_TEST_SRC_ERROR_HPP
#define STREAM9_ERRORS_TEST_SRC_ERROR_HPP

#include <system_error>

namespace testing {

enum class errc {
    error1, error2, error3,
};

inline std::error_category const&
error_category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept
        {
            return "test category";
        }

        std::string message(int const e) const
        {
            switch (static_cast<errc>(e)) {
                using enum errc;
                case error1:
                    return "error1";
                case error2:
                    return "error2";
                case error3:
                    return "error3";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

inline std::error_code
make_error_code(errc const e)
{
    return { static_cast<int>(e), error_category() };
}

} // namespace testing

namespace std {

template<>
struct is_error_code_enum<testing::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_ERRORS_TEST_SRC_ERROR_HPP
