#include <stream9/errors/find_nested_error.hpp>

#include "namespace.hpp"
#include "errors.hpp"
#include "error_code.hpp"

#include <stream9/errors/print_error.hpp>
#include <stream9/errors/auto_stacktrace.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(find_nested_error_)

BOOST_AUTO_TEST_CASE(with_type_)
{
    st9::set_auto_stacktrace(false);
    try {
        try {
            throw error1 { "test", errc::error1 };
        }
        catch (...) {
            std::throw_with_nested(error2 { "test", errc::error2 });
        }
    }
    catch (...) {
        auto e = err::find_nested_error<error1>();
        if (e) {
            std::ostringstream os;

            err::print_error(os, *e);

            auto expected = "../src/find_nested_error.cpp:32|error\n"
                            "../src/find_nested_error.cpp:21|test|error1\n";

            BOOST_TEST(os.str() == expected);
        }
    }
    st9::set_auto_stacktrace(true);
}

BOOST_AUTO_TEST_CASE(with_error_code_)
{
    st9::set_auto_stacktrace(false);
    try {
        try {
            throw error1 { "test", errc::error1 };
        }
        catch (...) {
            std::throw_with_nested(error2 { "test", errc::error2 });
        }
    }
    catch (...) {
        auto e = err::find_nested_error(std::current_exception(), errc::error1);
        if (e) {
            std::ostringstream os;

            err::print_error(os, *e);

            auto expected = "../src/find_nested_error.cpp:59|error\n"
                            "../src/find_nested_error.cpp:48|test|error1\n";

            BOOST_TEST(os.str() == expected);
        }
    }
    st9::set_auto_stacktrace(true);
}

BOOST_AUTO_TEST_SUITE_END() // find_nested_error_

} // namespace testing
