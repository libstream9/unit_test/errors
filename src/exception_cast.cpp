#include <stream9/errors/exception_cast.hpp>

#include "error_code.hpp"
#include "errors.hpp"
#include "namespace.hpp"

#include <stream9/errors/print_error.hpp>
#include <stream9/errors/auto_stacktrace.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(exception_cast_)

BOOST_AUTO_TEST_CASE(step1_)
{
    st9::set_auto_stacktrace(false);
    try {
        try {
            throw error1 { "error1", errc::error1 };
        }
        catch (...) {
            std::throw_with_nested(error2 { "error2", errc::error2 });
        }
    }
    catch (...) {
        auto e = err::exception_cast<error1 const*>();
        if (e) {
            std::ostringstream os;

            err::print_error(os, *e);

            auto expected = "../src/exception_cast.cpp:34|error\n"
                            "../src/exception_cast.cpp:23|error1|error1\n";

            BOOST_TEST(os.str() == expected);
        }
    }
    st9::set_auto_stacktrace(true);
}

template<typename> struct type_of;

BOOST_AUTO_TEST_CASE(same_type_)
{
    struct E1 {};
    struct E2 {};

    E1 e1;

    BOOST_TEST(err::exception_cast<E1*>(&e1));
    BOOST_TEST(err::exception_cast<E1 const*>(&e1));

    BOOST_TEST(!err::exception_cast<E2*>(&e1));
    BOOST_TEST(!err::exception_cast<E2 const*>(&e1));
}

BOOST_AUTO_TEST_CASE(to_base_class_)
{
    struct E1 {};
    struct E2 : E1 {};
    struct E3 {};

    E2 e2;
    E3 e3;

    BOOST_TEST(err::exception_cast<E1*>(&e2));
    BOOST_TEST(err::exception_cast<E1 const*>(&e2));

    BOOST_TEST(!err::exception_cast<E1*>(&e3));
    BOOST_TEST(!err::exception_cast<E1 const*>(&e3));
}

BOOST_AUTO_TEST_CASE(to_nested_type_)
{
    struct E1 {};
    struct E3 {};

    auto make_nested_exception = [](auto&& e) { //TODO extract
        try {
            throw e;
        }
        catch (...) {
            return std::nested_exception();
        }
    };

    E1 const e1;
    auto e2 = make_nested_exception(e1);

    BOOST_TEST(err::exception_cast<E1*>(&e2));
    BOOST_TEST(!err::exception_cast<E3*>(&e2));
}

BOOST_AUTO_TEST_SUITE_END() // exception_cast_

} // namespace testing
