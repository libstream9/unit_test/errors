#include <stream9/errors/rethrow_error.hpp>

#include "error_code.hpp"
#include "namespace.hpp"

#include <stream9/errors/error.hpp>
#include <stream9/errors/json.hpp>

#include <source_location>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>
#include <stream9/json/algorithm.hpp>

namespace testing {

static bool
print_mismatch(
    json::pointer const& p,
    json::value const& x1,
    json::value const& x2)
{
    std::cerr << "mismatch at " << p << ": "  << x1 << " vs " << x2 << std::endl;
    return false;
}

BOOST_AUTO_TEST_SUITE(rethrow_error_)

    BOOST_AUTO_TEST_CASE(without_context_)
    {
        std::source_location loc;

        try {
            try {
                loc = std::source_location::current();
                throw st9::error { errc::error1 };
            }
            catch (...) {
                st9::rethrow_error();
            }
        }
        catch (...) {
            auto result = json::value_from(std::current_exception());

            json::object expected {
                { "why", { { "message", "error1" } } },
                { "where", {
                    { "function name", loc.function_name() },
                    { "line", loc.line() + 1 },
                } },
            };

            BOOST_TEST(json::match(result, expected, print_mismatch));
        }
    }

    BOOST_AUTO_TEST_CASE(with_context_)
    {
        std::source_location loc;

        try {
            try {
                loc = std::source_location::current();
                st9::throw_error(errc::error1, { { "foo", 1 }, });
            }
            catch (...) {
                st9::rethrow_error({
                    { "bar", 2 },
                });
            }
        }
        catch (...) {
            auto result = json::value_from(std::current_exception());

            json::object expected {
                { "why", { { "message", "error1" } } },
                { "where", {
                    { "function name", loc.function_name() },
                    { "line", loc.line() + 1 },
                } },
                { "context", { { "foo", 1 }, { "bar", 2 } } },
            };

            BOOST_TEST(json::match(result, expected, print_mismatch));
        }
    }

BOOST_AUTO_TEST_SUITE_END() // rethrow_error_

} // namespace testing
