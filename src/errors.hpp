#ifndef STREAM9_ERRORS_TEST_SRC_ERRORS_HPP
#define STREAM9_ERRORS_TEST_SRC_ERRORS_HPP

#include "namespace.hpp"

#include <stream9/errors/error.hpp>

namespace testing {

template<typename T>
struct base_type : T
{
    using base_t = base_type;

    using T::T;
    using T::operator=;
};

struct error1 : base_type<err::error>
{
    using base_t::base_t;
    using base_t::operator=;
};

struct error2 : base_type<err::error>
{
    using base_t::base_t;
    using base_t::operator=;
};

struct error3 : base_type<err::error>
{
    using base_t::base_t;
    using base_t::operator=;
};

} // namespace testing

#endif // STREAM9_ERRORS_TEST_SRC_ERRORS_HPP
