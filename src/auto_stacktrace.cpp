#include <stream9/errors/auto_stacktrace.hpp>

#include <stream9/errors.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <execinfo.h>

namespace testing {

BOOST_AUTO_TEST_SUITE(auto_stacktrace_)

    void func3()
    {
        throw st9::error {
            st9::errc::invalid_argument, {
                { "detail", "whatever" },
            }
        };
    }

    void func2()
    {
        try {
            func3();
        }
        catch (...) {
            throw;
        }
    }

    void func1()
    {
        try {
            func2();
        }
        catch (...) {
            st9::rethrow_error();
        }
    }

    BOOST_AUTO_TEST_CASE(case_2_)
    {
        st9::set_auto_stacktrace(true);
        try {
            func1();
        }
        catch (...) {
            st9::print_error();
        }
        st9::set_auto_stacktrace(false);
    }

BOOST_AUTO_TEST_SUITE_END() // auto_stacktrace_

} // namespace testing
