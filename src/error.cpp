#include <stream9/errors/error.hpp>

#include "error_code.hpp"
#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(error_)

    using stream9::errors::error;

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(type_1_)
        {
            auto loc = std::source_location::current();
            error e { errc::error1 };

            BOOST_TEST(e.what() == loc.function_name());
            BOOST_TEST(e.why() == errc::error1);
            BOOST_TEST(e.where().file_name() == loc.file_name());
            BOOST_TEST(e.where().function_name() == loc.function_name());
            BOOST_TEST(e.where().line() == loc.line() + 1);
            BOOST_TEST(e.context().empty());
        }

        BOOST_AUTO_TEST_CASE(type_2_)
        {
            auto loc = std::source_location::current();
            error e { errc::error1, { { "foo", "" } } };

            BOOST_TEST(e.what() == loc.function_name());
            BOOST_TEST(e.why() == errc::error1);
            BOOST_TEST(e.where().file_name() == loc.file_name());
            BOOST_TEST(e.where().function_name() == loc.function_name());
            BOOST_TEST(e.where().line() == loc.line() + 1);
            BOOST_TEST(e.context().contains("foo"));
        }

        BOOST_AUTO_TEST_CASE(type_3_)
        {
            auto loc = std::source_location::current();
            error e { "what", errc::error1 };

            BOOST_TEST(e.what() == "what");
            BOOST_TEST(e.why() == errc::error1);
            BOOST_TEST(e.where().file_name() == loc.file_name());
            BOOST_TEST(e.where().function_name() == loc.function_name());
            BOOST_TEST(e.where().line() == loc.line() + 1);
            BOOST_TEST(e.context().empty());
        }

        BOOST_AUTO_TEST_CASE(type_4_)
        {
            auto loc = std::source_location::current();
            error e { "what", errc::error1, { { "context", "" } } };

            BOOST_TEST(e.what() == "what");
            BOOST_TEST(e.why() == errc::error1);
            BOOST_TEST(e.where().file_name() == loc.file_name());
            BOOST_TEST(e.where().function_name() == loc.function_name());
            BOOST_TEST(e.where().line() == loc.line() + 1);
            BOOST_TEST(e.context().contains("context"));
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_CASE(equality_)
    {
        error e { errc::error1 };

        BOOST_CHECK(e == e);
    }

BOOST_AUTO_TEST_SUITE_END() // error_

} // namespace testing
