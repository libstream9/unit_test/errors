#include <stream9/errors/print_error.hpp>

#include "error_code.hpp"
#include "errors.hpp"
#include "namespace.hpp"

#include <stream9/errors/error.hpp>
#include <stream9/errors/auto_stacktrace.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(print_error_)

BOOST_AUTO_TEST_CASE(step1_)
{
    st9::set_auto_stacktrace(false);
    err::error e { "test", errc::error1 };

    std::string s;
    str::ostream os { s };

    err::print_error(os, e);

    auto expected = "../src/print_error.cpp:28|error\n"
                    "../src/print_error.cpp:23|test|error1\n";

    BOOST_TEST(s == expected);
    st9::set_auto_stacktrace(true);
}

BOOST_AUTO_TEST_CASE(three_level_1_)
{
    st9::set_auto_stacktrace(false);
    try {
        try {
            throw "error1"sv;
        }
        catch (...) {
            try {
                std::throw_with_nested("error2"s);
            }
            catch (...) {
                std::throw_with_nested("error3"sv);
            }
        }
    }
    catch (...) {
        std::string s;
        str::ostream os { s };

        err::print_error(os);

        auto expected = "../src/print_error.cpp:57|error\n"
                        "error3\n"
                        "error2\n"
                        "error1\n";
        BOOST_TEST(s == expected);
    }
    st9::set_auto_stacktrace(true);
}

BOOST_AUTO_TEST_CASE(three_level_2_)
{
    st9::set_auto_stacktrace(false);
    try {
        try {
            throw error1 { "test", errc::error1 };
        }
        catch (...) {
            try {
                std::throw_with_nested(error2 { "test", errc::error2 });
            }
            catch (...) {
                std::throw_with_nested(error3 { "test", errc::error3 });
            }
        }
    }
    catch (...) {
        std::string s;
        str::ostream os { s };

        err::print_error(os);

        auto expected = "../src/print_error.cpp:88|error\n"
                        "../src/print_error.cpp:80|test|error3\n"
                        "../src/print_error.cpp:77|test|error2\n"
                        "../src/print_error.cpp:73|test|error1\n";

        BOOST_TEST(s == expected);
    }
    st9::set_auto_stacktrace(true);
}

BOOST_AUTO_TEST_SUITE_END() // print_error_

} // namespace testing
