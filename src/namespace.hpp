#ifndef STREAM9_ERRORS_TEST_SRC_NAMESPACE_HPP
#define STREAM9_ERRORS_TEST_SRC_NAMESPACE_HPP

namespace stream9::errors {}
namespace stream9::strings {}
namespace stream9::json {}

namespace testing {

namespace st9 { using namespace stream9; }
namespace err { using namespace stream9::errors; }
namespace str { using namespace stream9::strings; }
namespace json { using namespace stream9::json; }

} // namespace testing

#endif // STREAM9_ERRORS_TEST_SRC_NAMESPACE_HPP
